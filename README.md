# Find shortest path 
[Demo website](https://s3-ap-northeast-1.amazonaws.com/luopt/C/shortest_path/shortestPath_leaf.html)

## Build With
1. OSM map preprocess
    * [taiwan OSM Data](https://download.geofabrik.de/asia/taiwan.html) - Taiwan osm data
    * [Osmosis](https://wiki.openstreetmap.org/wiki/Osmosis) - Create a bounded osm data
    * [OsmToRoadGraph](https://github.com/AndGem/OsmToRoadGraph) - Convert osm data into road network(Include node and edge)
2. Build map
    * [leaflet](https://leafletjs.com)
3. Algorithm for shortest path
    * A star algorithm