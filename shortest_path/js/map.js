var token="pk.eyJ1IjoiZG1hcms0MjUiLCJhIjoiY2pmdGhwYjBiM2c0MDMwbnZqaXAwdnBiaCJ9.J8TxpBbtKsyIAqlK-Y-SqA";
var map = L.map('basicMap').setView([23.000012,120.216861],16)


L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw',{			
	maxZoom:18,
	id: 'mapbox.emerald',
	accessToken: token
}).addTo(map);
$(".zoom").html(map.getZoom());
var cur = map.getZoom();
map.on('zoom',()=>{
	const currentZoom = map.getZoom()
	$(".zoom").html(parseFloat(currentZoom).toFixed(1));				
});
var marked = false;

function onlocationFound(e){
	var location = e.latlng;
	newMarker = L.marker(location).addTo(map);
	markers.push(newMarker);
}

function getlocation(e){
	if(marked == false){
		map.on('locationfound',onlocationFound);
		marked = true;		
		map.locate({setView: true, maxZoom: 16});
		
	}				
}


var sidebar = L.control.sidebar('sidebar',{
	position: 'left',		
});
map.addControl(sidebar);
function getpath(){	
	var pathData = findpath();
	map.setView([pathData[1],pathData[0]],16);	
	spLayer = L.geoJSON(pathData[2]).addTo(map);
	spLayers.push(spLayer);
	condition = false;
}

function onMapClick(e){
	addMarker(e);
	var temp1 = $('input[name="home"]').val().length;
	var temp2 = $('input[name="dest"]').val().length;
	if(temp1 != 0 && temp2 != 0){
		$('input[name="home"]').val(null);
		$('input[name="dest"]').val(null);					
	}
	if($('input[name="home"]').val().length==0){
		$('input[name="home"]').val(e.latlng.lat.toPrecision(6)+","+e.latlng.lng.toPrecision(7));
		$('input[name="home_lat"]').val(e.latlng.lat);
		$('input[name="home_lng"]').val(e.latlng.lng);									
	}else{
		
		$('input[name="dest"]').val(e.latlng.lat.toPrecision(6)+","+e.latlng.lng.toPrecision(7));
		$('input[name="dest_lat"]').val(e.latlng.lat);
		$('input[name="dest_lng"]').val(e.latlng.lng);
	}
	
}
map.on('click',onMapClick);

var LeafIcon = L.Icon.extend({
    options: {
       iconSize:     [38, 95],
       shadowSize:   [50, 64],
       iconAnchor:   [22, 94],
       shadowAnchor: [4, 62],
       popupAnchor:  [-3, -76]
    }
});
var newMarker;
var markers = new Array();
var spLayers = new Array();
function addMarker(e){
// Add marker to map at click location; add popup window
	newMarker = new L.marker(e.latlng).addTo(map);
	markers.push(newMarker);
}
$(document).on('click','#searchpath', function(e) {
	pace.restart();
});
function f1(callback){	
	$(".loader").show();
	
	setTimeout(function(){
		
		callback();
		getpath();
		$(".loader").hide();
	},1000)
}
function f2(){	
	$(".loader").hide();
}

function deleteLayer(){
	for(var i = 0; i < spLayers.length; i++)
		map.removeLayer(spLayers[i]);
	for(var i = 0; i < markers.length; i++)
		map.removeLayer(markers[i]);
	$('input[name="home"]').val("");
	$('input[name="dest"]').val("");
	$('input[name="spdist"]').val("");
}

// create custom icon
var redMarker = L.icon({
    iconUrl: 'picture/red_marker.svg',
    iconSize: [38, 38], // size of the icon
    });
var blackMarker = L.icon({
    iconUrl: 'picture/black_marker.svg',
    iconSize: [38, 38], // size of the icon
    });
var blueMarker = L.icon({
    iconUrl: 'picture/blue_marker.png',
    iconSize: [38, 38], // size of the icon
    });
// L.geoJSON(node).addTo(map);

PaceOptions = {
	ajax: false,
	document: true,
	eventLag: true,
	elements: {
		selectors: ['#searchpath']
	}
};
function foodloader(callback){	
	$(".loader").show();	
	setTimeout(function(){		
		
		foodLayer();
		callback();
		$(".loader").hide();
	},1000)
}

function foodLayer(){	
	for(var i = 0; i < food.length; i++){
		if(food[i].category.includes("地方小吃"))
			newMarker = new L.marker([parseFloat(food[i].lat),parseFloat(food[i].long)], {icon: redMarker}).addTo(map).bindTooltip(food[i].name,{
	        permanent: true, 
	        direction: 'top'
	    }).bindPopup("<br>"+food[i].name+"</br>"+"<p>"+food[i].opentime+"<\p>"+"<p>"+food[i].tel+"<\p>"+"<p>"+food[i].summary+"<\p>");
		else if(food[i].category.includes("甜點糕餅") || food[i].category.includes("飲料冰品"))
			newMarker = new L.marker([parseFloat(food[i].lat),parseFloat(food[i].long)], {icon: blueMarker}).addTo(map).bindTooltip(food[i].name,{
	        permanent: true, 
	        direction: 'top'
	    }).bindPopup("<br>"+food[i].name+"</br>"+"<p>"+food[i].opentime+"<\p>"+"<p>"+food[i].tel+"<\p>"+"<p>"+food[i].summary+"<\p>");
		else if(food[i].category.includes("異國料理"))
			newMarker = new L.marker([parseFloat(food[i].lat),parseFloat(food[i].long)], {icon: blackMarker}).addTo(map).bindTooltip(food[i].name,{
	        permanent: true, 
	        direction: 'top'
	    }).bindPopup("<br>"+food[i].name+"</br>"+"<p>"+food[i].opentime+"<\p>"+"<p>"+food[i].tel+"<\p>"+"<p>"+food[i].summary+"<\p>");
		markers.push(newMarker);		
	}	
}