var edge_dict = [];
var condition = true;
for(var i = 0; i < nodelist.length;i++){
	edge_dict.push([]);
}
for(var i = 0; i < edgelist.length;i++){
	edge_dict[edgelist[i].from].push( 
	{
		to: edgelist[i].to,
		dist: edgelist[i].dist
	}
	);
}
var district = new Array(20);
for (var i = 0; i < 20; i++) {
  district[i] = new Array(20);
  for (var j = 0; j < 20; j++)
  	district[i][j] = [];
}

for(var k = 0; k < nodelist.length; k++){
	var row = parseInt((parseFloat(nodelist[k].lon) - 120.14)/0.01);	
	var col = parseInt((parseFloat(nodelist[k].lat) - 22.96)/0.005);

	district[row][col].push(parseInt(nodelist[k].idx));
}

function nearNode(){
	var home_lat = parseFloat($('input[name="home_lat"]').val());
	var home_lng = parseFloat($('input[name="home_lng"]').val());
	var dest_lat = parseFloat($('input[name="dest_lat"]').val());
	var dest_lng = parseFloat($('input[name="dest_lng"]').val());
	var h_row = parseInt((home_lng-120.14)/0.01);
	var h_col = parseInt((home_lat-22.96)/0.005);
	var d_row = parseInt((dest_lng-120.14)/0.01);
	var d_col = parseInt((dest_lat-22.96)/0.005);
	var hlist = district[h_row][h_col];
	var dlist = district[d_row][d_col];
	var Hsmallest = 10000;
	var Dsmallest = 10000;
	var home_idx = -1;
	var dest_idx = -1;
	var S = [];
	var dHome;
	var dDest;
	for(var i = 0; i < hlist.length;i++){

		dHome = Math.abs(home_lat - parseFloat(nodelist[hlist[i]].lat)) + Math.abs(home_lng - parseFloat(nodelist[hlist[i]].lon));		
			
		if(dHome < Hsmallest){
			Hsmallest = dHome;
			home_idx = parseFloat(nodelist[hlist[i]].idx);
		}		
	}
	
	for(var i = 0; i < dlist.length; i++){
		dDest = Math.abs(dest_lat - parseFloat(nodelist[dlist[i]].lat)) + Math.abs(dest_lng - parseFloat(nodelist[dlist[i]].lon));
		if(dDest < Dsmallest){
			Dsmallest = dDest;
			dest_idx = parseFloat(nodelist[dlist[i]].idx);
		}
	}
	
	S.push(home_idx);
	S.push(dest_idx);

	return S;
}


//
function findpath(){
	
	var S = nearNode();	
	// var d1 = new Date();
	// var t1 = d1.getTime();
	var data = Astar_search(edge_dict,S[0],S[1]);
	// var d2 = new Date();
	// var t2 = d2.getTime()
	// alert(t2-t1);
	// var data = dijkstra(edge_dict,S[0],S[1]);
	var result = data[0];

	$('input[name="spdist"]').val(data[1][S[1]].toPrecision(5));
	var sp_node = [];
	var spData = [];
	for(var i = 0; i < result.length; i++){
		var coor = [nodelist[result[i]].lon,nodelist[result[i]].lat];
		sp_node.push(coor);	
		if(i == 0){
			spData.push(nodelist[result[i]].lon);
			spData.push(nodelist[result[i]].lat);
		}
	}
	
	var path = {
		"type": "FeatureCollection",
		"features": [{
			"type": "Feature",
			"geometry": {    			
				"type": "LineString",        	
				"properties": {},
				"coordinates": sp_node					
			}
		}]
	};
	spData.push(path);

	return spData;
}



//
function dijkstra(edge_dict,source,sinknode){

	var dist = [];
	var pred = [];
	var Q = new Set();
	for(var v = 0; v < edge_dict.length; v++){
		dist.push(100000);
		pred.push(-1);
		if(v != source){
			Q.add(v);
		}
	}
	dist[source] = 0;
	pred[source] = -2;
	var S = new Set();
	S.add(source);
	
	for(var x=0;x < edge_dict[source].length;x++){
		if(dist[edge_dict[source][x].to] > dist[source]+edge_dict[source][x].dist){
			dist[edge_dict[source][x].to] = dist[source]+edge_dict[source][x].dist;
			pred[edge_dict[source][x].to] = source;
		}
	}
	while(Q.size != 0){
		var u = Extract_Min(Q,dist);
		if(u == -1){
			break;
		}else{
			Q.delete(u);
			S.add(u);
		}
		
		for(var x=0;x < edge_dict[u].length;x++){
			if(dist[edge_dict[u][x].to] > dist[u]+edge_dict[u][x].dist){
				dist[edge_dict[u][x].to] = dist[u]+edge_dict[u][x].dist;
				pred[edge_dict[u][x].to] = u;
			}
		}						
	}
	var order = []
	var k = sinknode;
	while(pred[k] != -2){
		order.push(k);
		k = pred[k];
		if(pred[k] == -2){
			order.push(k);
		}
	}
	var data = []
	data.push(order.reverse());
	data.push(dist[sinknode]);
	return data;
}
function Extract_Min(Q,dist){
	var min_dist = 100000;
	var min_idx = -1;
	for(var i = 0; i < dist.length;i++){
		if(Q.has(i) && dist[i]<min_dist){
			min_dist = dist[i];
			min_idx = i;
		}					
	}
	return min_idx;
}

// A* algorithm
function heuristic(a,b){
	return getDistanceFromLatLonInKm(parseFloat(nodelist[a].lat),parseFloat(nodelist[a].lon),parseFloat(nodelist[b].lat),parseFloat(nodelist[b].lon))*1000;
}
function getDistanceFromLatLonInKm(lat1,lon1,lat2,lon2) {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2-lat1);  // deg2rad below
  var dLon = deg2rad(lon2-lon1); 
  var a = 
    Math.sin(dLat/2) * Math.sin(dLat/2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * 
    Math.sin(dLon/2) * Math.sin(dLon/2)
    ; 
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
  var d = R * c; // Distance in km
  return d;
}

function deg2rad(deg) {
  return deg * (Math.PI/180)
}




// Heap Implement
const Htop = 0;
const parent = i => ((i + 1) >>> 1) - 1;
const left = i => (i << 1) + 1;
const right = i => (i + 1) << 1;

class PriorityQueue {
  constructor(comparator = (a, b) => a < b) {
    this._heap = [];
    this._comparator = comparator;
  }
  size() {
    return this._heap.length;
  }
  isEmpty() {
    return this.size() == 0;
  }
  peek() {
    return this._heap[Htop];
  }
  push(...values) {
    values.forEach(value => {
      this._heap.push(value);
      this._siftUp();
    });
    return this.size();
  }
  constain(value) {
  	var foundValue = false;
  	for(var idx = 0; idx < this.size(); idx++){
  		if(value == this._heap[idx][0])
  			foundValue = true;  	
  	}
  	return foundValue;
  }
  pop() {
    const poppedValue = this.peek();
    const bottom = this.size() - 1;
    if (bottom > Htop) {
      this._swap(Htop, bottom);
    }
    this._heap.pop();
    this._siftDown();
    return poppedValue;
  }
  replace(value) {
    const replacedValue = this.peek();
    this._heap[Htop] = value;
    this._siftDown();
    return replacedValue;
  }
  _greater(i, j) {
    return this._comparator(this._heap[i], this._heap[j]);
  }
  _swap(i, j) {
    [this._heap[i], this._heap[j]] = [this._heap[j], this._heap[i]];
  }
  _siftUp() {
    let node = this.size() - 1;
    while (node > Htop && this._greater(node, parent(node))) {
      this._swap(node, parent(node));
      node = parent(node);
    }
  }
  _siftDown() {
    let node = Htop;
    while (
      (left(node) < this.size() && this._greater(left(node), node)) ||
      (right(node) < this.size() && this._greater(right(node), node))
    ) {
      let maxChild = (right(node) < this.size() && this._greater(right(node), left(node))) ? right(node) : left(node);
      this._swap(node, maxChild);
      node = maxChild;
    }
  }
}

function Astar_search(edge_dict,source,sink){
	frontier = new PriorityQueue((a, b) => a[1] < b[1]);
	frontier.push([source,0]);
	var came_from = [];
	var cost_so_far = [];
	for(var v = 0; v < edge_dict.length; v++){
		came_from.push(-2);
		cost_so_far.push(1000000);
	}
	came_from[source] = -1;
	cost_so_far[source] = 0;
	while(!frontier.isEmpty()){		
		var current = frontier.pop()[0];		
		// alert("Current:"+current);
		// alert("sink"+sink);
		
		if(current == sink)
			break;
		for(var idx = 0; idx < edge_dict[current].length; idx++){			
			var next = edge_dict[current][idx];
			var new_cost = cost_so_far[current]+next.dist;
			if(cost_so_far[next.to] == 1000000 || new_cost < cost_so_far[next.to]){
				cost_so_far[next.to] = new_cost;					
				var priority = new_cost + heuristic(sink, next.to);
				// alert("Next: "+next.to)
				// alert("f:" + priority);
				frontier.push([next.to, priority]);
				
				came_from[next.to] = current;
			}
		}
	}
	var order = [];
	var k = sink;
	while(came_from[k] != -1){
		order.push(k);
		k = came_from[k];
		if(came_from[k] == -1){
			order.push(k);
		}
	}
	var result = [];
	result.push(order.reverse());
	result.push(cost_so_far);
	return result;
}
// Astar_search(edge_dict,0,3);